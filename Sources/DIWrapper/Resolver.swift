//
//  Resolver.swift
//  
//
//  Created by Andrey Petrovskiy on 02.12.2021.
//

import Foundation

public enum ResolverRegistrationType {
    case singleton
    case newInstance
    case moduleSingleton
}

public class Resolver {

    public static let shared = Resolver()
    private var factoryDict: [Bundle : [String: () -> Any]] = [:]
    private var factoryStaticDict: [Bundle : [String: () -> Any]] = [:]
    private var factorySingletonDict: [String: () -> Any] = [:]
    private var staticDict: [Bundle : [String: Any]] = [:]
    private var singletonDict: [String: Any] = [:]


    /// Register dependencies
    /// bundle = unoque identifier for dictionaries if you are working with ModuleArchitecture, by default is .main
    /// registration type = instance type registration
    public func add<T>(bundle: Bundle = .main,
                       registrationType: ResolverRegistrationType = .newInstance,
                       type: T.Type, _ factory: @escaping () -> T) {
        switch registrationType {
        case .newInstance:
            if factoryDict[bundle] != nil {
                factoryDict[bundle]?[String(describing: type.self)] = factory
            } else {
                factoryDict[bundle] = [String(describing: type.self) : factory]
            }
            factoryDict[bundle]?[String(describing: type.self)] = factory

        case .moduleSingleton:
            if factoryStaticDict[bundle] == nil {
                factoryStaticDict[bundle] = [:]
                staticDict[bundle] = [:]
            }
            factoryStaticDict[bundle]?[key(for: type.self)] = factory

        case .singleton:
            factorySingletonDict[key(for: type.self)] = factory

        }
    }

    public func resolve<T>(_ type: T.Type, registrationType: ResolverRegistrationType, bundle: Bundle = .main) -> T {
        switch registrationType {
        case .singleton:
            return resolveSingletone(type)
        case .newInstance:
            return resolveDynamic(type)
        case .moduleSingleton:
            return resolveModuleStatic(type, bundle: bundle)
        }
    }

    @discardableResult
    private func resolveDynamic<T>(_ type: T.Type) -> T {
        var component: T?
        factoryDict.forEach {
            if let object: T = $0.value[String(describing: T.self)]?() as? T {
                component = object
                return
            }
        }
        if let component = component {
            return component
        } else {
            fatalError("!!!!You has frogot to add \(String(describing: T.self))!!!!")
        }
    }

    @discardableResult
    private func resolveSingletone<T>(_ type: T.Type) -> T {
        var object: T?
        if let instance = singletonDict[String(describing: T.self)] as? T {
            object = instance
        } else if let instance = factorySingletonDict[key(for: T.self)]?() as? T {
            object = instance
            singletonDict[key(for: T.self)] = instance
        }

        guard let component = object else {
            fatalError("!!!!You has frogot to add \(key(for: T.self))!!!!")
        }

        return component
    }

    @discardableResult
    private func resolveModuleStatic<T>(_ type: T.Type, bundle: Bundle = .main) -> T {
        var object: T?
        if let instance = staticDict[bundle]?[key(for: T.self)] as? T {
            object = instance
        } else if let instance = factoryStaticDict[bundle]?[key(for: T.self)]?() as? T {
            object = instance
            staticDict[bundle]?[key(for: T.self)] = instance
        }

        guard let component = object else {
            fatalError("!!!!You has frogot to add \(key(for: T.self))!!!!")
        }

        return component
    }

    // MARK: - Clear module dependencies
    public func clearModuleDependecies(bundle: Bundle = .main) {
        staticDict[bundle] = [:]
    }

    public func clearSharedDependecies() {
        singletonDict = [:]
    }

    // MARK: - Private

    private func key<T>(for type: T.Type) -> String {
        let key = String(describing: T.self)
        return key
    }

}
