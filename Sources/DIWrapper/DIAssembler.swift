//
//  DIAssembler.swift
//  
//
//  Created by IEvgen Verkush on 22.03.2022.
//

import Foundation

public final class DIAssembler {

    private let resolver: Resolver

    public init(_ assemblies: [DIAssembly], resolver: Resolver = .shared) {
        self.resolver = resolver
        run(assemblies: assemblies)
    }

    public func apply(assembly: DIAssembly) {
        run(assemblies: [assembly])
    }

    public func apply(assemblies: [DIAssembly]) {
        run(assemblies: assemblies)
    }

    // MARK: Private

    private func run(assemblies: [DIAssembly]) {
        for assembly in assemblies {
            assembly.assemble(resolver: resolver)
        }
    }

}
