//
//  DIAssembly.swift
//  
//
//  Created by IEvgen Verkush on 22.03.2022.
//

import Foundation

public protocol DIAssembly {
    func assemble(resolver: Resolver)
}
